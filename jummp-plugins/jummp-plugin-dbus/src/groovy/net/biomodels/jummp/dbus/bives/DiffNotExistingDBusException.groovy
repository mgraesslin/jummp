/**
 * This file is part of the project bives.jummp, and thus part of the
 * implementation for the diploma thesis "Versioning Concepts and Technologies
 * for Biochemical Simulation Models" by Robert Haelke, Copyright 2010.
 */
package net.biomodels.jummp.dbus.bives

import org.freedesktop.dbus.exceptions.DBusExecutionException;

/**
 * //TODO add description for class DiffNotExistingDBusException.groovy
 * @author Robert Haelke, robert.haelke@googlemail.com
 * @date 22.07.2011
 * @year 2011
 */
class DiffNotExistingDBusException extends DBusExecutionException {

	/**
	 * @param arg0
	 */
	public DiffNotExistingDBusException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
