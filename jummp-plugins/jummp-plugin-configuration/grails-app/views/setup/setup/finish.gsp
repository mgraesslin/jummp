<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>Setup</title>
    </head>
    <body>
        <div id="remote" class="body">
            <h1>Setup Finished</h1>
            <div>
                <p style="margin-left: 12px;">
                    The setup process is finished. Redeploy you web archive to activate the changes.
                </p>
            </div>
        </div>
    </body>
</html>
