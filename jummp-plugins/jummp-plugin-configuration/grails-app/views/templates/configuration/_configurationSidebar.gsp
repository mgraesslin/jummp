<content tag="sidebar">
    <div class="element">
        <h1>Configuration Modules</h1>
        <h2></h2>
        <p>
            <ul>
                <li><a href="${createLink(action: 'database')}">Database</a></li>
                <li><a href="${createLink(action: 'ldap')}">LDAP</a></li>
                <li><a href="${createLink(action: 'svn')}">Subversion</a></li>
                <li><a href="${createLink(action: 'vcs')}">Version Control System</a></li>
                <li><a href="${createLink(action: 'remote')}">Remote</a></li>
                <li><a href="${createLink(action: 'server')}">Server</a></li>
                <li><a href="${createLink(action: 'bives')}">Model Versioning System - BiVeS</a></li>
                <li><a href="${createLink(action: 'userRegistration')}">User Registration</a></li>
                <li><a href="${createLink(action: 'changePassword')}">Change/Reset Password</a></li>
                <li><a href="${createLink(action: 'branding')}">Select Branding</a></li>
            </ul>
        </p>
    </div>
</content>
