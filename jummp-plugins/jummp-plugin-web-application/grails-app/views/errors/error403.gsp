<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="main" />
        <title><g:message code="error.403.title"/></title>
    </head>
    <body>
        <h1><g:message code="error.403.title"/></h1>
        <p><g:message code="error.403.explanation"/>
    </body>
</html>
