<tr>
    <th class="ruleTitle"><g:message code="sbml.rules.rateRule"/></th>
    <td class="ruleValue"><span><sbml:rateRuleMath variable="${variable}"><jummp:contentMathML mathML="${math}"/></sbml:rateRuleMath></span></td>
</tr>
