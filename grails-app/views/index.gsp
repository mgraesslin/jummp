<!doctype html>
<html>
    <head>
        <meta name="layout" content="main"/>
    </head>
    <body activetab="about">
        <wcm:render path="index"/>
    </body>
    <content tag="sidebar">
        <wcm:render path="main-sidebar"/>
    </content>
</html>
