<!-- Render the page title -->
<title>${node.titleForHTML.encodeAsHTML()}</title>
<link rel="shortcut icon" href="${g.createLink(uri: '/images/favicon.ico')}"/>
<less:stylesheet name="jummp"/>
<less:scripts />
<r:require module="jquery"/>
<r:layoutResources/>
