<div id="nav">
  <div class="left"><a href="${g.createLink(uri: '/')}"><g:message code="jummp.main.tabs.about"/></a></div>
  <div class="right"><a href="#"><g:message code="jummp.main.tabs.search"/></a></div>
</div>
