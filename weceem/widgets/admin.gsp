<div class="element">
  <h1>Administration</h1>
  <h2></h2>
  <ul>
    <li><g:link controller="wcm-admin">Content Management System</g:link></li>
    <li><g:link controller="configuration">System Configuration</g:link></li>
    <li><g:link controller="userAdministration">User Administration</g:link></li>
    <li><g:link controller="miriam">Miriam Administration</g:link></li>
  </ul>
</div>
