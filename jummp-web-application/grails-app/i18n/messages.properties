default.doesnt.match.message=Property [{0}] of class [{1}] with value [{2}] does not match the required pattern [{3}]
default.invalid.url.message=Property [{0}] of class [{1}] with value [{2}] is not a valid URL
default.invalid.creditCard.message=Property [{0}] of class [{1}] with value [{2}] is not a valid credit card number
default.invalid.email.message=Property [{0}] of class [{1}] with value [{2}] is not a valid e-mail address
default.invalid.range.message=Property [{0}] of class [{1}] with value [{2}] does not fall within the valid range from [{3}] to [{4}]
default.invalid.size.message=Property [{0}] of class [{1}] with value [{2}] does not fall within the valid size range from [{3}] to [{4}]
default.invalid.max.message=Property [{0}] of class [{1}] with value [{2}] exceeds maximum value [{3}]
default.invalid.min.message=Property [{0}] of class [{1}] with value [{2}] is less than minimum value [{3}]
default.invalid.max.size.message=Property [{0}] of class [{1}] with value [{2}] exceeds the maximum size of [{3}]
default.invalid.min.size.message=Property [{0}] of class [{1}] with value [{2}] is less than the minimum size of [{3}]
default.invalid.validator.message=Property [{0}] of class [{1}] with value [{2}] does not pass custom validation
default.not.inlist.message=Property [{0}] of class [{1}] with value [{2}] is not contained within the list [{3}]
default.blank.message=Property [{0}] of class [{1}] cannot be blank
default.not.equal.message=Property [{0}] of class [{1}] with value [{2}] cannot equal [{3}]
default.null.message=Property [{0}] of class [{1}] cannot be null
default.not.unique.message=Property [{0}] of class [{1}] with value [{2}] must be unique

default.paginate.prev=Previous
default.paginate.next=Next
default.boolean.true=True
default.boolean.false=False
default.date.format=yyyy-MM-dd HH:mm:ss z
default.number.format=0

default.created.message={0} {1} created
default.updated.message={0} {1} updated
default.deleted.message={0} {1} deleted
default.not.deleted.message={0} {1} could not be deleted
default.not.found.message={0} not found with id {1}
default.optimistic.locking.failure=Another user has updated this {0} while you were editing

default.home.label=Home
default.list.label={0} List
default.add.label=Add {0}
default.new.label=New {0}
default.create.label=Create {0}
default.show.label=Show {0}
default.edit.label=Edit {0}

default.button.create.label=Create
default.button.edit.label=Edit
default.button.update.label=Update
default.button.delete.label=Delete
default.button.delete.confirm.message=Are you sure?

# Data binding errors. Use "typeMismatch.$className.$propertyName to customize (eg typeMismatch.Book.author)
typeMismatch.java.net.URL=Property {0} must be a valid URL
typeMismatch.java.net.URI=Property {0} must be a valid URI
typeMismatch.java.util.Date=Property {0} must be a valid Date
typeMismatch.java.lang.Double=Property {0} must be a valid number
typeMismatch.java.lang.Integer=Property {0} must be a valid number
typeMismatch.java.lang.Long=Property {0} must be a valid number
typeMismatch.java.lang.Short=Property {0} must be a valid number
typeMismatch.java.math.BigDecimal=Property {0} must be a valid number
typeMismatch.java.math.BigInteger=Property {0} must be a valid number

# jQuery internationalization
# dataTables
jquery.dataTables.empty=No data available
jquery.dataTables.info=Showing {0} to {1} of {2} entries
jquery.dataTables.infoEmpty=
jquery.dataTables.infoFiltered=(filtered from ${0} total entries)
jquery.dataTables.lengthMenu=Show {0} entries
jquery.dataTables.processing=Processing…
jquery.dataTables.search=Search:
jquery.dataTables.noFilterResults=No matching records found
# paginate
jquery.dataTables.paginate.first=First
jquery.dataTables.paginate.last=Last
jquery.dataTables.paginate.next=Next
jquery.dataTables.paginate.previous=Previous

# Messages used in Login form
login.username=Login ID
login.password=Password
login.authenticate=Login
login.cancel=Cancel
login.register=No Account yet? Feel free to <a href="#" onclick="{0}">Register</a> your own account.
login.passwordForgotten=Password Forgotten
logout.title=Logout
login.successful=Login successful.
logout.successful=Logout successful.

# Messages used in Model list view
model.list.title=List of all Models
model.list.modelId=Model ID
model.list.name=Name
model.list.publicationId=Publication ID
model.list.lastModificationDate=Last Modified
model.list.format=Model Format
model.list.table.download=Download
model.list.goto=Show Model List

# messages for upload
model.upload.file=File
model.upload.name=Model Name
model.upload.comment=Comment
model.upload.publication.section=Publication
model.upload.pubmed=PubMed
model.upload.doi=DOI
model.upload.url=Custom URL
model.upload.unpublished=Unpublished
model.upload.success=New Model with Id {0} created successfully.
model.upload.goto=Upload a Model
# messages for upload errors
model.upload.error.file=You have to upload a Model file!
model.upload.error.name.blank=The Name of the Model may not be empty!
model.upload.error.comment.blank=You have to specify a comment!
model.upload.error.pubmed.blank=Insert a PubMed ID for the Publication!
model.upload.error.pubmed.numeric=The PubMed Id has to be numeric!
model.upload.error.doi.blank=Insert a DOI for the Publication!
model.upload.error.url.blank=Insert an URL to a Publication!
model.upload.error.url.invalid=Enter a valid URL to a Publication!
model.upload.error.publicationTitle.blank=You have to specify the title of the Publication!
model.upload.error.publicationJournal.blank=You have to specify the Journal of the Publication!
model.upload.error.publicationAffiliation.blank=You have to specify the affiliation of the Publication!
model.upload.error.publicationAbstract.maxSize=The Abstract may not be longer than 1000 characters!
model.upload.error.publicationAbstract.blank=You have to specify the abstract of the Publication!
model.upload.error.publicationYear.blank=You have at least to specify the year of the Publication!
model.upload.error.publicationYear.range=Select the Publication year from the dropdown list!
model.upload.error.publicationMonth.inList=Select the Publication month from the dropdown list!
model.upload.error.publicationDay.range=Select the Publication day from the dropdown list!
model.upload.error.publicationDay.blank=The selected day is not valid for the selected month and year!
model.upload.error.authorInitials.blank=At least one Author is required. Please specify the author's initials!
model.upload.error.authorFirstName.blank=At least one Author is required. Please specify the author's first name!
model.upload.error.authorLastName.blank=At least one Author is required. Please specify the author's last name!
# messages for add revision
model.show.back=Back to Model
model.revision.upload.goto=Upload a new Revision
model.revision.upload.success=New Model Revision for Model {0} uploaded successfully.
model.revision.minor=This is a minor revision.
model.revision.uploadedBy=Uploaded on {0} by {1}.
model.revision.delete.button=Delete
model.revision.delete.verify=Are you sure you want to delete this Model Revision? In case it is the last revision, the complete Model would be deleted. Please note that the Model files will be kept in the version control system.
model.revision.delete.success=Revision deleted
model.revision.delete.error=Revision could not be deleted
# messages for reactiongraph
model.reactiongraph.title=Reaction Graph
# messages for octave output
model.octave.title=Octave File
# messages for BioPAX output
model.bioPax.title=BioPAX File

# Publication
publication.pubmedid=PubMed ID
publication.doi=DOI
publication.journal=Journal
publication.issue=Issue
publication.volume=Volume
publication.pages=Pages
publication.date=Date
publication.affiliation=Affiliation
publication.abstract=Abstract
publication.authors=Authors
publication.search.pubmed=Search Medline
publication.search.doi=Resolve a DOI
publication.title=Title
# Publication Upload
publication.upload.year=Select Year
publication.upload.month=Select Month (optional)
publication.upload.day=Select Day (optional)
publication.upload.authors=Authors
publication.upload.author.initials=Initials
publication.upload.author.firstName=First Name
publication.upload.author.lastName=Last Name
publication.upload.ui.addAuthor=Add Author
publication.upload.ui.removeAuthor=Remove Author

# Model Summary
model.view.header={0} - {1} (Revision #{2})
model.summary.reference-publication=Reference Publication
model.summary.reference-publication-unpublished=No Reference Publication
model.summary.information=Model
model.summary.model.id=Model Id
model.summary.model.submitter=Model Submitter
model.summary.model.submissionDate=Submission Date
model.summary.model.revision.id=Revision Id
model.summary.model.revision.uploadDate=Revision Upload Date
model.summary.model.revision.submitter=Revision Submitter
model.summary.model.format=Model Format
model.summary.model.download=Download
model.summary.model.creators=Encoders
model.summary.notes=Notes
model.summary.annotation=Annotation

# Overview
model.overview.math=Mathematical Expressions
model.overview.entities=Physical entities
model.overview.parameters=Global Parameters
model.overview.compartments.species=Species

# theme change view
theme.change.select=Select a Theme
theme.change.button=Change Theme
theme.change.success=Theme changed successfully to {0}.
theme.save.error.blank=Select a Theme from the List!
theme.save.error.invalid=The selected Theme is not valid!

# generic error messages
error.unknown=Unknown error occurred while processing {0}!
error.500.title=Unexpected error occurred
error.500.explanation=The performed action could not be processed by the server. The unique error Id is {0}.
error.403.title=Access Denied
error.403.explanation=Sorry, you are not authorized to perform the requested operation. In case you are not logged-in, please login and try again.
error.404.title=Resource not found
error.404.explanation=Sorry, the requested resource {0} is not available.

# date messages
date.month.january=January
date.month.february=February
date.month.march=March
date.month.april=April
date.month.may=May
date.month.june=June
date.month.july=July
date.month.august=August
date.month.september=September
date.month.october=October
date.month.november=November
date.month.december=December

# User Edit
user.change.oldPassword.blank=Please enter the old Password!
user.change.newPassword.blank=Please enter the new Password!
user.change.verifyPassword.invalid=The new Password and the Verification does not match!
user.change.oldPassword.incorrect=The old Password is incorrect, Password not changed!
user.change.password.success=The password was changed successfully. Please use the new password in next login.
user.change.password.disabled=Changing password is not supported by this instance.
user.change.ui.oldPassword=Old Password
user.change.ui.newPassword=New Password
user.change.ui.verifyPassword=Verify Password
user.change.ui.changePassword=Change Password
user.change.ui.username=Login
user.change.ui.realname=Name
user.change.ui.email=Email
user.change.ui.heading.user=User
user.change.ui.heading.password=Change Password
user.edit.userRealName.blank=Please specify your name!
user.edit.email.blank=Please specify your email address!
user.edit.email.invalid=Please specify a valid email address!
user.edit.success=User Information updated successfully.
user.edit.unchanged=User could not be updated.
# reset password
user.resetPassword.ui.explanation=To request a new password enter your Login ID and click "Request New Password". You will receive an email with a link to reset your password.
user.resetPassword.ui.requestPassword=Request New Password
user.resetPassword.ui.requested=The request to reset the password has been received. An email with further processing steps has been sent to your email address. Please check your inbox.
user.resetPassword.error.userNotFound=Could not find user with this login id. No email sent.
user.resetPassword.ui.reset=Reset Password
user.resetPassword.ui.header=Reset Password
user.resetPassword.error.username.blank=Please specify your username!
user.resetPassword.error.password.blank=Please specify a password!
user.resetPassword.error.code.blank=The unique code is required! Don't mess around with the form!
user.resetPassword.ui.success=Your password has been changed. Please proceed by logging in.
# User Administration
user.administration.list.id=Id
user.administration.list.username=Username
user.administration.list.realname=Real Name
user.administration.list.email=Email
user.administration.list.enabled=Enabled
user.administration.list.accountExpired=Account Expired
user.administration.list.accountLocked=Account Locked
user.administration.list.passwordExpired=Password Expired
user.administration.userRole.error.roleNotFound=The selected Role could not be found anymore. Please reload the view.
user.administration.userRole.error.userNotFound=The selected User could not be found anymore. Please reload the view.
user.administration.userRole.error.general=Error occurred during processing of action.
user.administration.userRole.success=Role for selected User updated successfully.
user.administration.userRole.ui.addRole=Add Role to User
user.administration.userRole.ui.removeRole=Remove Role from User
user.administration.userRole.ui.heading=Manage Roles for User <em>{0}</em>
user.administration.userRole.ui.heading.usersRoles=Roles assigned to User
user.administration.userRole.ui.heading.availableRoles=Available Roles
user.administration.register.error.username.blank=Please specify the username for the new user!
user.administration.register.error.email.blank=Please specify the email address of the new user! The registration link will be sent to this address.
user.administration.register.error.email.invalid=Please specify a valid email address! The registration link will be sent to this address.
user.administration.register.error.userRealName.blank=Please specify the name of the new user!
user.administration.register.success=New User with Id {0} created.
# User Registration
user.register.username=Username
user.register.password=Password
user.register.verifyPassword=Verify Password
user.register.email=Email
user.register.realName=Name
user.register.ui.register=Register
user.register.error.username.blank=Please specify your username!
user.register.error.password.blank=Please specify a password!
user.register.error.password.invalid=Please specify a password!
user.register.error.email.blank=Please specify an email address!
user.register.error.email.invalid=Please specify a valid email address!
user.register.error.verifyPassword.invalid=Password and verification do not match!
user.register.error.userRealName.blank=Please specify your name!
user.register.ui.title=User Registration
user.register.success=New User registered successfully. Depending on the installation settings you will receive a confirmation mail or an administrator has to validate the account.
user.register.disabled=User Registration is not possible in this instance. Please contact an administrator for registration.
user.register.validate.ui.title=Validate User Registration
user.register.validate.ui.explanation=In order to perform the registration please enter your registered login name.
user.register.validate.success=Your account has been successfully enabled. Please proceed by using the login with your registered user name and password.
user.register.confirm.error.username.blank=Please specify your username!
user.register.confirm.error.password.blank=Please specify a password!
user.register.confirm.error.password.invalid=Please specify a password!
user.register.confirm.error.verifyPassword.invalid=Password and Verification do not match!
user.register.confirm.error.code.blank=Confirmation code is invalid! Please verify that you copied the complete URL from the email.
user.register.confirm.ui.title=Confirm User Account
user.register.confirm.ui.explanation=Please enter your username and specify a password for your account.
user.register.confirm.ui.explanation.noPassword=Please enter your username to activate your account. You can later on login with your LDAP password.

# ui elements
ui.button.cancel=Cancel
ui.button.upload=Upload
ui.button.save=Save
ui.button.update=Update
ui.button.register=Register
ui.button.validate=Validate
ui.button.showDiff=Show diff
ui.button.schedule=Schedule

# sbml
sbml.qualifier.bqb.encodes=encodes
sbml.qualifier.bqb.hasPart=has part
sbml.qualifier.bqb.hasProperty=has property
sbml.qualifier.bqb.hasVersion=has version
sbml.qualifier.bqb.is=is
sbml.qualifier.bqb.isDescribedBy=is described by
sbml.qualifier.bqb.isEncodedBy=is encoded by
sbml.qualifier.bqb.isHomologTo=is homolog to
sbml.qualifier.bqb.isPartOf=is part of
sbml.qualifier.bqb.isPropertyOf=is property of
sbml.qualifier.bqb.isVersionOf=is version of
sbml.qualifier.bqb.occursIn=occurs in
sbml.qualifier.bqm.is=is
sbml.qualifier.bqm.isDerivedFrom=is derived from
sbml.qualifier.bqm.isDescribedBy=is described by
sbml.parameters.globalParameters=Global Parameters
sbml.parameters.title={0} ({1})
sbml.parameters.value=Value: {0}
smbl.parameters.unit=(Unit: {0})
sbml.parameters.constant=Constant
sbml.reactions.title=Reactions
sbml.event.trigger.title=Trigger:
sbml.event.delay.title=Delay:
sbml.rules.title=Rules
sbml.rules.rateRule=Rate Rule
sbml.rules.assignmentRule=Assignment Rule
sbml.rules.algebraicRule=Algebraic Rule
sbml.functionDefinitions.title=Functions
sbml.compartments.title=Compartments
smbl.compartments.spatialDimensions=Spatial Dimensions: {0}
sbml.compartments.size=Size: {0}
smbl.species.title=Species: {0}
smbl.species.initialAmount=Initial Amount: {0}
smbl.species.initialConcentration=Initial Concentration: {0}
smbl.species.substanceUnits=Substance Units: {0}
smbl.species.initialAmount.tableRow.title=Initial Amount:
smbl.species.initialConcentration.tableRow.title=Initial Concentration:
smbl.species.substanceUnits.tableRow.title=Substance Units:
smbl.species.title.overview=Species:

# MIRIAM Administration
miriam.update.ui.url=URL to MIRIAM XML:
miriam.update.ui.force=Delete all previously loaded MIRIAM data:
miriam.update.success=Update of MIRIAM resources scheduled. Please check Server log files whether the update was successful.
miriam.data.update.success=Update of all MIRIAM Information scheduled. This may take a while.
miriam.data.update=Trigger an update of all fetched MIRIAM data. This will resolve all names again and update them to a new name. It will not fetch new annotations from the Model. The process will be executed in the background.
miriam.model.update=Schedule all models for processing the MIRIAM annotations. For each not yet resolved annotation it will be tried to resolve. Only the latest revision of the Model is used.
miriam.model.update.success=All Models have been scheduled for MIRIAM data resolving. This may take a while.
miriam.update.url.invalid=You have to specify an URL to the MIRIAM resource description!
miriam.update.error=Error occurred during update of MIRIAM resources. Please verify that the URL is pointing to a MIRIAM description XML.

# general
sbo.tableRow.title=SBO:
annotations.tableRow.title=Annotations:
notes.tableRow.title=Notes:
math.tableRow.title=Math:

# bives
bives.overview.headline=Diff summary
bives.revision.recent=Previous revision:
bives.revision.previous=Recent revision:
bives.overview.modifications=Modifications:
bives.overview.modifications.move=move(s)
bives.overview.modifications.update=update(s)
bives.overview.modifications.insert=insert(s)
bives.overview.modifications.delete=delete(s)
bives.modifications.elementsInRevision=Elements in revision
bives.modifications.moves=Moves
bives.modifications.updates=Updates
bives.modifications.inserts=Insert
bives.modifications.deletes=Delete