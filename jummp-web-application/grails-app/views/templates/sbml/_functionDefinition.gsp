<tr rel="${metaLink}" title="${title}">
    <th class="functionDefinitionTitle">${title}</th>
    <td class="functionDefinitionValue"><jummp:contentMathML mathML="${math}"/></td>
</tr>
