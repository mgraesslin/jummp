<span class="eventAssignment">
    <span class="eventVariable">${variable}</span>
    <span class="eventEquals">=</span>
    <span class="eventMath"><jummp:contentMathML mathML="${math}"/></span>
</span>
