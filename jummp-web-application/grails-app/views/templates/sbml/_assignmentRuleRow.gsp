<tr>
    <th class="ruleTitle"><g:message code="sbml.rules.assignmentRule"/></th>
    <td class="ruleValue"><span><sbml:assignmentRuleMath variable="${variable}"><jummp:contentMathML mathML="${math}"/></sbml:assignmentRuleMath></span></td>
</tr>
