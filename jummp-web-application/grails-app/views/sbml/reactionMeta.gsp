<table>
    <jummp:contentMathMLTableRow mathML="${reaction.math}"/>
    <jummp:sboTableRow sbo="${reaction.sbo}"/>
    <jummp:annotationsTableRow annotations="${reaction.annotation}"/>
    <sbml:notesTableRow notes="${reaction.notes}"/>
</table>
