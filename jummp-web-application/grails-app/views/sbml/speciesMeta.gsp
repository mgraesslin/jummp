<table>
    <sbml:initialAmountTableRow initialAmount="${species.initialAmount}"/>
    <sbml:initialConcentrationTableRow initialConcentration="${species.initialConcentration}"/>
    <sbml:substanceUnitsTableRow substanceUnits="${species.substanceUnits}"/>
    <jummp:sboTableRow sbo="${species.sbo}"/>
    <jummp:annotationsTableRow annotations="${species.annotation}"/>
    <sbml:notesTableRow notes="${species.notes}"/>
</table>
